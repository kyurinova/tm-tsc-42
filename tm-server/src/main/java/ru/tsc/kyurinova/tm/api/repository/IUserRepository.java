package ru.tsc.kyurinova.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.dto.model.UserDTO;

import java.util.Comparator;
import java.util.List;

public interface IUserRepository {

    @Insert("INSERT INTO tm_user (row_id, login, password_hash, email, fst_name, last_name, mid_name, role, locked)" +
            " VALUES (#{user.id}, #{user.login}, #{user.passwordHash}, #{user.email}, #{user.firstName}," +
            " #{user.lastName}, #{user.middleName}, #{user.role}, #{user.locked})")
    void add(
            @NotNull @Param("user") UserDTO user
    );

    @Select("SELECT * FROM tm_user WHERE login = #{login}")
    @Result(column = "row_id", property = "id")
    @Result(column = "password_hash", property = "passwordHash")
    @Result(column = "fst_name", property = "firstName")
    @Result(column = "last_name", property = "lastName")
    @Result(column = "mid_name", property = "middleName")
    @Nullable
    UserDTO findByLogin(
            @NotNull @Param("login") String login
    );

    @Select("SELECT * FROM tm_user WHERE email = #{email}")
    @Result(column = "row_id", property = "id")
    @Result(column = "password_hash", property = "passwordHash")
    @Result(column = "fst_name", property = "firstName")
    @Result(column = "last_name", property = "lastName")
    @Result(column = "mid_name", property = "middleName")
    @Nullable
    UserDTO findByEmail(
            @NotNull @Param("email") String email
    );

    @Update("UPDATE tm_user SET password_hash = #{password} WHERE row_id = #{userId}")
    void setPassword(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("password") String password
    );

    @Update("UPDATE tm_user SET fst_name = #{firstName}, last_name = #{lastName}, mid_name = #{middleName}" +
            " WHERE row_id = #{userId}")
    void updateUser(
            @NotNull @Param("userId") String userId,
            @NotNull @Param("firstName") String firstName,
            @NotNull @Param("lastName") String lastName,
            @NotNull @Param("middleName") String middleName
    );

    @Update("UPDATE tm_user SET locked = #{lockFlag} WHERE login = #{login}")
    void setLockedByLogin(
            @NotNull @Param("login") String login,
            @NotNull @Param("lockFlag") String lockFlag
    );

    @Delete("DELETE FROM tm_user WHERE row_id = #{id}")
    void removeById(
            @NotNull @Param("id") String id
    );

    @Delete("DELETE FROM tm_user WHERE login = #{login}")
    void removeByLogin(
            @NotNull @Param("login") String login
    );

    @Delete("DELETE FROM tm_user WHERE row_id = #{user.id}")
    void remove(
            @NotNull @Param("user") final UserDTO user
    );

    @Select("SELECT * FROM tm_user")
    @Result(column = "row_id", property = "id")
    @Result(column = "password_hash", property = "passwordHash")
    @Result(column = "fst_name", property = "firstName")
    @Result(column = "last_name", property = "lastName")
    @Result(column = "mid_name", property = "middleName")
    @Nullable
    List<UserDTO> findAll(
    );

    @Select("SELECT * FROM tm_user ORDER BY #{comporator}")
    @Result(column = "row_id", property = "id")
    @Result(column = "password_hash", property = "passwordHash")
    @Result(column = "fst_name", property = "firstName")
    @Result(column = "last_name", property = "lastName")
    @Result(column = "mid_name", property = "middleName")
    @Nullable
    List<UserDTO> findAllComporator(
            @NotNull @Param("comparator") Comparator comparator
    );

    @Delete("DELETE FROM tm_user")
    void clear(
    );

    @Select("SELECT * FROM tm_user where row_id = #{id}")
    @Result(column = "row_id", property = "id")
    @Result(column = "password_hash", property = "passwordHash")
    @Result(column = "fst_name", property = "firstName")
    @Result(column = "last_name", property = "lastName")
    @Result(column = "mid_name", property = "middleName")
    @Nullable
    UserDTO findById(
            @NotNull @Param("id") final String id
    );

    @Select("SELECT * FROM tm_user LIMIT 1 OFFSET #{index}")
    @Result(column = "row_id", property = "id")
    @Result(column = "password_hash", property = "passwordHash")
    @Result(column = "fst_name", property = "firstName")
    @Result(column = "last_name", property = "lastName")
    @Result(column = "mid_name", property = "middleName")
    @Nullable
    @NotNull
    UserDTO findByIndex(
            @NotNull @Param("index") final Integer index
    );

    @Delete("DELETE FROM tm_user WHERE row_id in" +
            " (SELECT row_id from tm_user LIMIT 1 OFFSET #{index})")
    void removeByIndex(
            @NotNull @Param("index") final Integer index
    );

    @Select("SELECT count(*) FROM tm_user")
    int getSize(
    );

}
