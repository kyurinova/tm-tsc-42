package ru.tsc.kyurinova.tm.dto.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractOwnerEntityDTO extends AbstractEntityDTO {

    @Nullable
    @Column(name = "user_id")
    protected String userId;

}
