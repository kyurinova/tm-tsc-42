package ru.tsc.kyurinova.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.endpoint.IAdminUserEndpoint;
import ru.tsc.kyurinova.tm.api.service.ISessionService;
import ru.tsc.kyurinova.tm.api.service.IUserService;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.dto.model.SessionDTO;
import ru.tsc.kyurinova.tm.dto.model.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class AdminUserEndpoint implements IAdminUserEndpoint {

    private IUserService userService;

    private ISessionService sessionService;

    public AdminUserEndpoint() {
    }

    public AdminUserEndpoint(@NotNull IUserService userService, @NotNull ISessionService sessionService) {
        this.userService = userService;
        this.sessionService = sessionService;
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void removeUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @WebParam(name = "entity", partName = "entity")
                    UserDTO entity
    ) {
        sessionService.validate(session, Role.ADMIN);
        userService.remove(entity);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void clearUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session
    ) {
        sessionService.validate(session, Role.ADMIN);
        userService.clear();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void removeByIdUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        sessionService.validate(session, Role.ADMIN);
        userService.removeById(id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void removeByIndexUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        sessionService.validate(session, Role.ADMIN);
        userService.removeByIndex(index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void removeByLoginUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "login", partName = "login")
                    String login
    ) {
        sessionService.validate(session, Role.ADMIN);
        userService.removeByLogin(login);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull UserDTO createUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "login", partName = "login")
                    String login,
            @Nullable
            @WebParam(name = "password", partName = "password")
                    String password
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.create(login, password);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull UserDTO createAdminUser(
            @Nullable
            @WebParam(name = "login", partName = "login")
                    String login,
            @Nullable
            @WebParam(name = "password", partName = "password")
                    String password
    ) {
        try {
            return userService.findByLogin(login);
        } catch (@NotNull final Exception e) {
            return userService.createAdmin(login, password);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull UserDTO createUserEmail(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "login", partName = "login")
                    String login,
            @Nullable
            @WebParam(name = "password", partName = "password")
                    String password,
            @Nullable
            @WebParam(name = "email", partName = "email")
                    String email
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.create(login, password, email);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull UserDTO createUserRole(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "login", partName = "login")
                    String login,
            @Nullable
            @WebParam(name = "password", partName = "password")
                    String password,
            @Nullable
            @WebParam(name = "role", partName = "role")
                    Role role
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.create(login, password, role);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable UserDTO setPasswordUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "userId", partName = "userId")
                    String userId,
            @Nullable
            @WebParam(name = "password", partName = "password")
                    String password
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.setPassword(userId, password);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable UserDTO updateUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "userId", partName = "userId")
                    String userId,
            @Nullable
            @WebParam(name = "firstName", partName = "firstName")
                    String firstName,
            @Nullable
            @WebParam(name = "lastName", partName = "lastName")
                    String lastName,
            @Nullable
            @WebParam(name = "middleName", partName = "middleName")
                    String middleName
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.updateUser(userId, firstName, lastName, middleName);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable UserDTO lockUserByLogin(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "login", partName = "login")
                    String login
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.lockUserByLogin(login);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable UserDTO unlockUserByLogin(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "login", partName = "login")
                    String login
    ) {
        sessionService.validate(session, Role.ADMIN);
        return userService.unlockUserByLogin(login);
    }

}
