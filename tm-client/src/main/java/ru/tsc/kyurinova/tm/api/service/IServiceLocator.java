package ru.tsc.kyurinova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kyurinova.tm.endpoint.*;

public interface IServiceLocator {

    @NotNull
    ISessionService getSessionService();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    AdminDataEndpoint getAdminDataEndpoint();

    @NotNull
    AdminUserEndpoint getAdminUserEndpoint();

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    TaskEndpoint getTaskEndpoint();

    @NotNull
    SessionEndpoint getSessionEndpoint();

    @NotNull
    UserEndpoint getUserEndpoint();

    @NotNull
    ProjectTaskEndpoint getProjectTaskEndpoint();

}
